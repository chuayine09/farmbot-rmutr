import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationComponent } from './components/navigation/navigation.component';
import { LayoutModule } from '@angular/cdk/layout';
import { ThirdPartyModule } from './3rd.module';
import { AppService } from './app.service';
import { AngularFireModule } from '@angular/fire';
import {
  AngularFireDatabaseModule,
  AngularFireDatabase,
} from '@angular/fire/database';

@NgModule({
  declarations: [AppComponent, NavigationComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    ThirdPartyModule,
    AngularFireModule.initializeApp({
      apiKey: 'AIzaSyDOAdkbBYWtuap5oY9kjwOByfVTiZpVYm8',
      authDomain: 'jirapat-dev.firebaseapp.com',
      databaseURL: 'https://jirapat-dev.firebaseio.com',
      projectId: 'jirapat-dev',
      storageBucket: 'jirapat-dev.appspot.com',
      messagingSenderId: '253044157111',
      appId: '1:253044157111:web:6fc89a627eb7c845e8fb32',
      measurementId: 'G-SMVE0NY7F0',
    }),
    AngularFireDatabaseModule,
  ],
  providers: [AppService, AngularFireDatabase],
  bootstrap: [AppComponent],
})
export class AppModule {}
