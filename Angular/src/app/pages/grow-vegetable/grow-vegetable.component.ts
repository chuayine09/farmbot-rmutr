import { AfterViewInit, Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { environment } from 'src/environments/environment';

const _window: any = window;

@Component({
  selector: 'app-grow-vegetable',
  templateUrl: './grow-vegetable.component.html',
  styleUrls: ['./grow-vegetable.component.scss'],
})
export class GrowVegetableComponent implements OnInit, AfterViewInit {
  public listArr = [
    { img: `${environment.baseHref}assets/vegetables/3039988.svg` },
    { img: `${environment.baseHref}assets/vegetables/2909782.svg` },
    { img: `${environment.baseHref}assets/vegetables/765544.svg` },
  ];
  public tableList: Array<any> = [];
  public positionVegetable: number[] = [];

  constructor(private afDB: AngularFireDatabase) {}

  ngAfterViewInit() {
    this.initialData();

    this.afDB
      .object('/tblListOfGrow')
      .snapshotChanges()
      .subscribe((val) => {
        let grow = val.payload.val();
        let tableList = document.querySelectorAll('.tableList'),
          elem: HTMLDivElement,
          img: HTMLImageElement,
          block: HTMLDivElement;

        Object.keys(grow).forEach((key: string) => {
          this.positionVegetable[parseInt(key)] = parseInt(grow[key]);

          img = new Image(50);
          img.src = this.listArr[parseInt(grow[key])].img;

          block = document.createElement('div');
          block.className = 'vegetables';
          block.setAttribute('vegetable-id', `${key}`);
          block.appendChild(img);

          elem = tableList.item(parseInt(key)) as HTMLDivElement;
          elem.innerHTML = '';
          elem.appendChild(block);
        });
      });
  }

  ngOnInit(): void {
    this.positionVegetable.forEach((e: number, index: number) => {});
  }

  private setTableList = (
    tableIndex: number,
    vegetableIndex: number = null,
    vagetableElement: any = null
  ) => {
    // this.tableList[tableIndex] = {
    //   vegetableIndex: vegetableIndex,
    //   vagetableElement: vagetableElement,
    // };

    this.positionVegetable[tableIndex] = vegetableIndex;
    this.afDB.object('/tblListOfGrow').set(this.positionVegetable);
  };

  private initialData = () => {
    let vegetables = null,
      tableList = null,
      that = this,
      fillSelected = null;

    initialHtml();
    function initialHtml() {
      vegetables = document.querySelectorAll('.vegetables');
      tableList = document.querySelectorAll('.tableList');

      vegetables.forEach((vegetable) => {
        vegetable.addEventListener('dragstart', dragStart);
        vegetable.addEventListener('dragend', dragEnd);
      });

      tableList.forEach((table) => {
        table.addEventListener('dragover', dragOver);
        table.addEventListener('dragenter', dragEnter);
        table.addEventListener('dragleave', dragLeave);
        table.addEventListener('drop', dragDrop);
      });
    }

    function dragStart() {
      fillSelected = this;
    }

    function dragEnd() {
      this.className = 'vegetables';
      fillSelected = null;
      initialHtml();
    }

    function dragOver(e) {
      e.preventDefault();
    }

    function dragEnter(e) {
      this.className += ' hovered';
      e.preventDefault();
    }

    function dragLeave(e) {
      e.preventDefault();
      this.className = 'tableList';
    }

    function dragDrop(e) {
      e.preventDefault();
      let vegetable_id: number = parseInt(
        `${fillSelected.getAttribute('vegetable-id')}`
      );
      let table_position: number =
        parseInt(`${this.getAttribute('table-position')}`) - 1;

      if (this.childNodes.length > 0) {
        this.removeChild(this.childNodes[0]);
      }
      var $html = _window.$(fillSelected);
      var temp = document.createElement('div');
      temp.innerHTML = $html.prop('outerHTML');
      this.appendChild(temp);
      that.setTableList(table_position, vegetable_id, temp);
      this.className = 'tableList';
    }
  };
}
